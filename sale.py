# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_invoice_line(self):
        party = None
        if self.sale and self.sale.party:
            party = self.sale.party
        elif self.party:
            party = self.party

        company = self.sale.company
        partner = (party in company.party.partners)
        with Transaction().set_context(partner=partner):
            return super().get_invoice_line()
