# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    partners = fields.Function(
        fields.Many2Many('party.party', None, None, 'Partners'),
        'get_partners', searcher='search_partners')

    def get_partners(self, name=None):
        return [p.id for p in self.party.partners]

    @classmethod
    def search_partners(cls, name, clause):
        return [('party.%s' % clause[0], ) + tuple(clause[1:])]
